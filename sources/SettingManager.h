//
// Created by ovishnevskiy on 10/14/19.
//

#pragma once

#include <iostream>
#include <vector>
#include "Defines.h"


class ConfigValues {
public:
	ConfigValues() : ScreenWidth(0), ScreenHeight(0), difficult(0),
					 WallCollision(true), GraphicEngine("SDL") {
	    std::cout << "Hey bitch" << std::endl;
	}


	uint16_t    ScreenWidth;
	uint16_t    ScreenHeight;
	uint16_t    difficult;
	bool        WallCollision;
	std::string GraphicEngine;

	void printVar() {
		std::cout << ScreenWidth << " " << ScreenHeight << " " << difficult << " " << WallCollision << " "
		          << GraphicEngine << std::endl;
	}
};

class SettingManager {

public:
	SettingManager() = default;
	~SettingManager() = default;
    ConfigValues GetConfigVariables() noexcept;
	void SetConfigVariables(ConfigValues &conf);

private:
	std::vector<std::pair<std::string, std::string>> Conf;
	bool ReadFile();
	bool WriteToFile(ConfigValues &config);
	static bool SelfCheck(ConfigValues &config);
	const std::string setting_path = "./conf/setting.cf";
};
