//
// Created by ovishnevskiy on 10/30/19.
//

#pragma once

#include "SettingManager.h"
#include <memory>
#include "Models/Food.h"
#include "Models/Snake.h"


class GameLogic {

public:

	GameLogic();
	~GameLogic() = default;

	[[nodiscard]] bool GameOver() const;
	[[nodiscard]] std::string GetCoreLibrary() const;
	void SetCoreLibrary(std::string) noexcept;

	void LoadSetting(ConfigValues);
	std::pair<int32_t, int32_t> GenerateRandomPosition();
private:
	std::shared_ptr<Snake>        _Snake {nullptr};
	std::shared_ptr<ConfigValues> _GameSetting;
	std::shared_ptr<GameLogic>    _GameInstance;
	std::shared_ptr<Food>         _Fruit;
};
