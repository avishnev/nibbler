#pragma once

#include <iostream>
#include <exception>
#include "IGraphicLib.h"
#include "SettingManager.h"
#include <memory>

#include "Defines.h"
#include "LibLoader.h"
#include "GameLogic.h"
#include "DrawBuffer.h"

class GameEngine
{
public:

    void InitGameLogic();
    void LoadGraphicLibrary();
    void CreateMap(std::uint16_t, std::uint16_t);
    void StartGameLoop();
    GameEngine() = default;
    ~GameEngine() = default;

private:
    bool LoadSetting() noexcept;
    bool InitModels();

	std::string     LibraryPath = "./sources/shared/libogl";
	ConfigValues    GameSetting = ConfigValues();
	SettingManager  ConfigManager;

	DrawBuffer drawBuffer;
    std::unique_ptr<GameLogic> Logic;
	std::unique_ptr<LibLoader<IGraphicLib, DrawBuffer>> Engine;
//    std::unique_ptr<LibLoader<IGraphicLib, DrawBuffer>> engine;

};
