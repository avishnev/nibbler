//
// Created by Alexey VISHNEVSKY on 2019-10-09.
//

#pragma once

#include <memory>
#include "SettingManager.h"
#include "GameLogic.h"
#include "DrawBuffer.h"

class IGraphicLib {
public:
    virtual void Draw() = 0;
    virtual ~IGraphicLib() = default;

	virtual bool EventChecker() = 0;

//    typedef IGraphicLib* init_t(std::shared_ptr<ConfigValues>&);
    typedef IGraphicLib* init_t(std::shared_ptr<DrawBuffer>&);
    typedef void* cleanup_t(IGraphicLib*);
};
