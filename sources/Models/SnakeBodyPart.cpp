//
// Created by ovishnevskiy on 10/29/19.
//

#include "SnakeBodyPart.h"

SnakeBodyPart::SnakeBodyPart(int32_t x, int32_t y, bool head) :
								GameObject(x, y, ObjectType::SNAKE_BODY), _isHead(head) {}

bool SnakeBodyPart::IsHead() const { return _isHead; }