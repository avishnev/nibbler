//
// Created by ovishnevskiy on 10/28/19.
//

#ifndef NIBBLER_FOOD_H
#define NIBBLER_FOOD_H

#pragma once

#include "GameObject.h"

class Food : public GameObject {
public:
	Food(int32_t x, int32_t y);
	~Food() override = default;
	void SpawnFood(int32_t x, int32_t y) noexcept ;

private:
	bool isSpawned = false;
};


#endif //NIBBLER_FOOD_H
