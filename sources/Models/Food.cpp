//
// Created by ovishnevskiy on 10/28/19.
//

#include "Food.h"

Food::Food(int32_t x, int32_t y) : GameObject(x, y , ObjectType::FOOD) { }

void Food::SpawnFood(int32_t x, int32_t y) noexcept {
	if (!isSpawned) {
		SetX(x);
		SetY(y);
		isSpawned = true;
	}
}