//
// Created by ovishnevskiy on 10/29/19.
//

#ifndef NIBBLER_SNAKEBODYPART_H
#define NIBBLER_SNAKEBODYPART_H

#include "GameObject.h"

class SnakeBodyPart : public GameObject {
public:
	SnakeBodyPart(int32_t x, int32_t y, bool head);
	~SnakeBodyPart() override = default;

	bool IsHead() const ;

private:
	bool _isHead;
};


#endif //NIBBLER_SNAKEBODYPART_H
