//
// Created by ovishnevskiy on 10/28/19.
//

#include "Snake.h"
#include "SnakeBodyPart.h"
#include <iostream>

Snake::Snake(int32_t x, int32_t y)
{
	if (x < 2 || y < 2)
		std::cout << x << y << std::endl;
	auto head = SnakeBodyPart(x, y,  true);
	auto tail = SnakeBodyPart(x - 1, y, false);

	SnakeBody.push_back(head);
	SnakeBody.push_back(tail);
	FaceDirection = Direction::RIGHT;
}

std::list<SnakeBodyPart> &Snake::GetSnakeBody() { return SnakeBody; }

Direction   Snake::GetDirection() { return FaceDirection; }
void        Snake::SetDirection(Direction direction) { FaceDirection = direction; }

