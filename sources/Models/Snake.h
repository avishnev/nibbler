//
// Created by ovishnevskiy on 10/28/19.
//
#pragma once
#include <list>
#include "GameObject.h"
#include "SnakeBodyPart.h"


enum class Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

class Snake
{
public:
	explicit Snake(int32_t x, int32_t y);

	~Snake() = default;
	Direction GetDirection();
	void SetDirection(Direction);
	[[nodiscard]] std::list<SnakeBodyPart> &GetSnakeBody();

private:
	[[maybe_unused]] Direction FaceDirection = Direction::RIGHT;
	std::list<SnakeBodyPart> SnakeBody;
};
