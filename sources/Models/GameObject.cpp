//
// Created by ovishnevskiy on 10/28/19.
//

#include "GameObject.h"

GameObject::GameObject(int32_t _x, int32_t _y, ObjectType _type): x(_x), y(_y), type(_type)  { }

int32_t GameObject::GetX() { return x; }
int32_t GameObject::GetY() { return y; }

ObjectType GameObject::GetObjectType() { return type; }

void GameObject::SetX(int32_t var) { x = var; }
void GameObject::SetY(int32_t var) { y = var; }