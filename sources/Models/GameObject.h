//
// Created by ovishnevskiy on 10/28/19.
//
#pragma once

#include <memory>

enum class ObjectType {
	WALL,
	SNAKE_BODY,
	FOOD,
	SURFACE
};

class GameObject {
public:
	explicit GameObject(int32_t x, int32_t y, ObjectType type);
	virtual ~GameObject() = default;

	ObjectType  GetObjectType();
	int32_t     GetX();
	int32_t     GetY();

protected:
	void SetX(int32_t);
	void SetY(int32_t);

private:
	int32_t     x;
	int32_t     y;
	ObjectType  type;
};

