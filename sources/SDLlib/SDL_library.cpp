//
// Created by Alexey VISHNEVSKY on 2019-10-08.
//


#include "SDL_library.h"

class DrawBuffer;

extern "C" IGraphicLib *Execute(std::shared_ptr<DrawBuffer> &sharedData) {
	auto *graphicInterface = new GraphicEngine_SDL(sharedData);

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {

		std::cerr << SDL_GetError() << std::endl;
		delete graphicInterface;
		return nullptr;
	}

	graphicInterface->setWindow( SDL_CreateWindow( "Nibbler", SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED, sharedData->getGameSetting()->ScreenWidth,
			 sharedData->getGameSetting()->ScreenHeight, SDL_WINDOW_ALLOW_HIGHDPI ));

	if (!graphicInterface->getWindow()) {
		std::cerr << SDL_GetError() << std::endl;
		exit (EXIT_FAILURE);
	}

	graphicInterface->createRender(SDL_CreateRenderer( graphicInterface->getWindow(), -1,
			SDL_RENDERER_ACCELERATED));

	return graphicInterface;
}
extern "C" void Destroy(GraphicEngine_SDL *ptr) {
	delete ptr;
}

bool GraphicEngine_SDL::EventChecker() {
	SDL_Event e;

	while (SDL_PollEvent(&e)) {
		if ((e.type == SDL_QUIT) || e.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
			return false;
		if (e.key.state == SDL_PRESSED) {
			switch (e.key.keysym.scancode) {
				case SDL_SCANCODE_UP :
					std::cout << "UP pressed" << std::endl;
					break;
			    case SDL_SCANCODE_DOWN :
					std::cout << "DOWN pressed" << std::endl;
					break;
				case SDL_SCANCODE_LEFT :
					std::cout << "LEFT pressed" << std::endl;
					break;
				case SDL_SCANCODE_RIGHT :
					std::cout << "RIGHT pressed" << std::endl;
					break;
				default:
					break;
			}
		}
	}
	return true;
}


void GraphicEngine_SDL::Draw() {

	SDL_Surface *image = SDL_LoadBMP("../sprites/life.jpg");

	if (image)
		std::cout << image->refcount << std::endl;
	SDL_RenderClear(renderer);
	DrawMap();
	DrawFruit();
	DrawShake();
	SDL_RenderPresent(renderer);
}

GraphicEngine_SDL::GraphicEngine_SDL(std::shared_ptr<DrawBuffer> &GameSetting) {

	SDL_version compiled;
	SDL_version linked;

	SDL_VERSION(&compiled);
	SDL_GetVersion(&linked);
	printf("Compiled against SDL version %d.%d.%d ...\n",
	       compiled.major, compiled.minor, compiled.patch);
	printf("Linking against SDL version %d.%d.%d.\n",
	       linked.major, linked.minor, linked.patch);

	instance = GameSetting;
}

GraphicEngine_SDL::~GraphicEngine_SDL() {
	SDL_DestroyWindow(MainWindow);
	SDL_Quit();
}

void GraphicEngine_SDL::DrawFruit() {

	SDL_Rect fruit {instance->getFood()->GetX(),instance->getFood()->GetY(), 30, 30};
	SDL_RenderDrawRect(renderer, &fruit);
}

void GraphicEngine_SDL::DrawShake() {

	auto body = instance->getSnake()->GetSnakeBody();

	for (auto var : body)
	{
		SDL_Rect tmp = {var.GetX(), var.GetY(), 30, 30};
		SDL_RenderFillRect(renderer, &tmp);
	}
}

void GraphicEngine_SDL::DrawMap() {
	if (instance->getGameSetting()->WallCollision) {
		SDL_SetRenderDrawColor(renderer, 255, 255,255, SDL_ALPHA_OPAQUE);
		SDL_Rect wall[2] = {
			{1, 1,(instance->getGameSetting()->ScreenWidth * 2 - 2),(instance->getGameSetting()->ScreenHeight * 2 - 2)},
			{2, 2,(instance->getGameSetting()->ScreenWidth * 2 - 2),(instance->getGameSetting()->ScreenHeight * 2 - 2)}
		};

		SDL_RenderDrawRects(renderer, wall, 2);
	}

}

bool GraphicEngine_SDL::setWindow(SDL_Window *window) {
	if (!MainWindow)
		MainWindow = window;
	return MainWindow == nullptr;
}

SDL_Window *GraphicEngine_SDL::getWindow() {
	return MainWindow;
}

bool GraphicEngine_SDL::createRender(SDL_Renderer *render) {

	renderer = render;

	if (!renderer) {
		std::cerr << SDL_GetError() << std::endl;
		exit(EXIT_FAILURE);
	}
	return false;
}
