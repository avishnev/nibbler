//
// Created by Alexey VISHNEVSKY on 2019-10-08.
//

#pragma once

#include "../IGraphicLib.h"
//#include "../SettingManager.h"
#include "../DrawBuffer.h"
#include <SDL.h>
#include <iostream>
#include <memory>

class DrawBuffer;

class GraphicEngine_SDL : public IGraphicLib {

public:
	explicit GraphicEngine_SDL(std::shared_ptr<DrawBuffer> &);
	~GraphicEngine_SDL() override;

	bool        setWindow(SDL_Window *);
	bool        createRender(SDL_Renderer *);
	SDL_Window  *getWindow();

	bool EventChecker() override;
	void Draw()         override;

private:

	void DrawMap();
	void DrawShake();
	void DrawFruit();

	SDL_Window      *MainWindow    {nullptr};
//	SDL_Surface     *surface   {nullptr};
	SDL_Renderer    *renderer  {nullptr};
	std::shared_ptr<DrawBuffer> instance;
};
