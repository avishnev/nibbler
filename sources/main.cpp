#include <iostream>
#include <cstring>
#include <memory>

#include "GameEngine.hpp"
#include "CustomExceptions.h"

static void Usage(const char *ProgramName) {
    std::cout << "Usage: " << ProgramName << " [width] [height]" << std::endl;
}

static bool ValidValue(const char *val)
{
	for (size_t i = 0; i < strlen(val); ++i ) {
		if (!isdigit(val[i]))
			return false;
	}
	return true;
}

int main(int ac, const char **av)
{
	if (ac != 3) {
        PRINT_ERROR("Wrong usage")
		Usage(av[0]);
		return (-1);
	}
	if (!ValidValue(av[1]) || !ValidValue(av[2])) {
        PRINT_ERROR("Data isn`t numbers")
        return -2;
    }

	auto Engine = std::make_unique<GameEngine>();

	try {
        Engine->CreateMap(std::stoll(av[1]), std::stoll(av[2]));
		Engine->InitGameLogic();
		Engine->LoadGraphicLibrary();
        Engine->StartGameLoop();
    } catch (EngineException &e) {
        PRINT_ERROR(e.what())
	}
   return 0;
}
