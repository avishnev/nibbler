
#include "GameEngine.hpp"
#include "CustomExceptions.h"
#include "LibLoader.h"

#include <dlfcn.h>

static bool validNumber(std::int32_t val) {
    return (val >= WIND_MIN_SIZE && val <= WIND_MAX_SIZE);
}

void GameEngine::CreateMap(std::uint16_t width, std::uint16_t height) {
    if (!validNumber(width) || !validNumber(height))
        throw EngineException("Invalid Window Params");
	if (!LoadSetting())
		throw EngineException("Can`t load setting");

    GameSetting.ScreenHeight = height;
	GameSetting.ScreenWidth = width;

	try {
		ConfigManager.SetConfigVariables(GameSetting);

	} catch (SettingException &e) {
		PRINT_ERROR(e.what())
	} catch (...) {
		exit(EXIT_FAILURE);
	}
}

bool GameEngine::LoadSetting() noexcept {
	GameSetting = ConfigManager.GetConfigVariables();
    return true;
}

//TODO make Main menu: with setting section (read/write ->conf/setting.cf)
void GameEngine::LoadGraphicLibrary() {

	if (GameSetting.GraphicEngine == "SDL")
		LibraryPath = "./sources/shared/libsdl";
	else if (GameSetting.GraphicEngine == "OpenGL")
		LibraryPath = "./sources/shared/libogl";
	else if (GameSetting.GraphicEngine == "SMFL")
		LibraryPath = "./sources/shared/libsmfl";

	Engine = std::make_unique<LibLoader<IGraphicLib, DrawBuffer>> (GameEngine::LibraryPath, "Execute", "Destroy");
//    engine = std::make_unique<LibLoader<IGraphicLib, GameLogic>> (GameEngine::LibraryPath, "Execute", "Destroy");

    if (!Engine->loadLib())
        throw EngineException(dlerror());
//    if (!engine->loadLib())
//        throw EngineException(dlerror());

}

void GameEngine::StartGameLoop() {

	auto GameData = std::make_shared<DrawBuffer>(drawBuffer);

	auto Gui = Engine->getInstance(GameData);

	Gui->Draw();
	while (Gui->EventChecker());

//	if (Engine->closeLib())
//		throw EngineException(dlerror());

}

void GameEngine::InitGameLogic() {
	Logic = std::make_unique<GameLogic>();
	Logic->LoadSetting(GameSetting);
	GameEngine::InitModels();
}

bool GameEngine::InitModels() {
	auto pos = Logic->GenerateRandomPosition();

	Food food(pos.first, pos.second);
	Snake snake(GameSetting.ScreenHeight / 2, GameSetting.ScreenHeight / 2);


	drawBuffer.LoadFoodModel(food);
	drawBuffer.LoadGameSetting(GameSetting);
	drawBuffer.LoadSnakeModel(snake);


	return false;
}
