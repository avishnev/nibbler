//
// Created by ovishnevskiy on 10/17/19.
//
#pragma once

#define RED "\033[0;31m"
#define NC "\033[0m"
#define YELLOW "\033[0;33m"

#define PRINT_ERROR(x) std::cerr << RED << (x) << NC << std::endl;
#define PRINT_WARNING(x) std::cerr << YELLOW << (x) << NC << std::endl;


#define WIND_MIN_SIZE 480
#define WIND_MAX_SIZE 1920
#define NIGTHMARE_MODE 5
