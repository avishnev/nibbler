//
// Created by ovishnevskiy on 10/15/19.
//
#pragma once

#include <SFML/Graphics.hpp>
#include "../IGraphicLib.h"
#include "../SettingManager.h"
#include <memory>

class SMFL_library : public IGraphicLib {
public:
	explicit SMFL_library(std::shared_ptr<ConfigValues> &config);
	~SMFL_library() override ;
	void Draw() override;
	bool EventChecker() override;

	void SetWindow(sf::RenderWindow*);
	[[nodiscard]] sf::RenderWindow *getWindow() const;


private:
	void DrawMap();
	void DrawShake();
	void DrawFruit();

	sf::RenderWindow* window;
	std::shared_ptr <ConfigValues> instanse;
};
