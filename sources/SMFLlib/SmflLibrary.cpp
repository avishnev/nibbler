//
// Created by ovishnevskiy on 10/15/19.
//

#include <iostream>
#include "SmflLibrary.h"

extern "C" IGraphicLib *Execute(std::shared_ptr<ConfigValues> &config) {

	auto *GraphicInterface = new SMFL_library(config);
// posible leak
	GraphicInterface->SetWindow(new sf::RenderWindow(sf::VideoMode(
		static_cast<unsigned int>(config->ScreenWidth),
		static_cast<unsigned int>(config->ScreenHeight)),
		"Nibbler")
		);
	if (!GraphicInterface->getWindow()) {
		sf::err() << "Cant create window" << std::endl;
		exit(EXIT_FAILURE);
	}
	return GraphicInterface;
}
extern "C" void Destroy(SMFL_library *ptr) {
	delete ptr;
}


void SMFL_library::Draw() {
	std::cout << "Hi form " << __func__ << std::endl;
	DrawMap();
	DrawFruit();
	DrawShake();
	window->display();

}

SMFL_library::SMFL_library(std::shared_ptr<ConfigValues> &GameSetting) {
	GameSetting->printVar();
	instanse = GameSetting;
}

void SMFL_library::DrawMap() {
	if (instanse->WallCollision) {
		sf::RectangleShape r1(sf::Vector2f(window->getSize().x - 1, window->getSize().y - 1));
		r1.setPosition(1, 1);
		window->draw(r1);
	}
}

void SMFL_library::DrawShake() {

}

void SMFL_library::DrawFruit() {

}

bool SMFL_library::EventChecker() {
	sf::Event e;

	while (window->pollEvent(e)){
		if (e.type == sf::Event::Closed || e.key.code == sf::Keyboard::Escape) {
			return false;
		} else if (sf::Event::KeyPressed) {
			switch (e.key.code) {
				case sf::Keyboard::Up :
					std::cout << "Up pressed" << std::endl;
					break;
				case sf::Keyboard::Down :
					std::cout << "Down pressed" << std::endl;
					break;
				case sf::Keyboard::Left :
					std::cout << "Left pressed" << std::endl;
					break;
				case sf::Keyboard::Right :
					std::cout << "Right pressed" << std::endl;
					break;
				case sf::Keyboard::Unknown:
					break;
				default:
					break;
			}
		}

	}
	return true;
}

void SMFL_library::SetWindow(sf::RenderWindow* _window) {
	window = _window;
}

sf::RenderWindow *SMFL_library::getWindow() const {
	return window;
}

SMFL_library::~SMFL_library() {
	window->close();
}
