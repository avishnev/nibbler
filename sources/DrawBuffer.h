//
// Created by Oleksii on 11/26/19.
//

#pragma once

#include <memory>
#include "Models/Snake.h"
#include "Models/Food.h"
#include "SettingManager.h"

class DrawBuffer {
public:

	DrawBuffer() = default;
	~DrawBuffer() = default;

	void LoadSnakeModel(const Snake&) noexcept;
	void LoadGameSetting(const ConfigValues&) noexcept;
	void LoadFoodModel(const Food&) noexcept;
	std::shared_ptr<Food> getFood() {return _Fruit;}
	std::shared_ptr<Snake> getSnake() {return _Snake;}
	std::shared_ptr<ConfigValues> getGameSetting() { return _GameSetting;}
private:
	std::shared_ptr<Food>           _Fruit        {nullptr};
	std::shared_ptr<Snake>          _Snake        {nullptr};
	std::shared_ptr<ConfigValues>   _GameSetting  {nullptr};
};
