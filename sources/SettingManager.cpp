//
// Created by ovishnevskiy on 10/14/19.
//

#include <fstream>
#include "SettingManager.h"
#include "CustomExceptions.h"


bool SettingManager::ReadFile() {
	std::string buff;
	std::ifstream file(setting_path);

    if (file.is_open() && !file.eof()) {
        while (std::getline(file, buff)) {
        	if (buff[0] != '#') {
		        Conf.emplace_back(
				        buff.substr(0, buff.find(':')),
				        buff.substr(buff.find(':') + 1, buff.size())
		        );
        	}
	        else continue;

        }
        file.close();
    } else
        return false;
    return true;
}

bool SettingManager::WriteToFile(ConfigValues &conf) {

	std::ofstream file(setting_path);

	if (file.is_open()) {
		for (const auto& var: Conf) {
			if (var.first == "ScreenWidth")
				file << var.first << ":" << conf.ScreenWidth << std::endl;
			if (var.first == "ScreenHeight")
				file << var.first << ":" << conf.ScreenHeight << std::endl;
			if (var.first == "Difficult")
				file << var.first << ":" << conf.difficult << std::endl;
			if (var.first == "GraphicLib")
				file << var.first << ":" << conf.GraphicEngine << std::endl;
			if (var.first == "WallCollision")
				file << var.first << ":" << conf.WallCollision << std::endl;
		}
		file.close();
	}

	return true;
}

ConfigValues SettingManager::GetConfigVariables() noexcept {

    ConfigValues config = ConfigValues();
	SettingManager::ReadFile();

	for (const auto& var : Conf) {
		if (var.first == "ScreenWidth")     config.ScreenWidth = std::stoi(var.second);
		if (var.first == "ScreenHeight")    config.ScreenHeight = std::stoi(var.second);
		if (var.first == "Difficult")       config.difficult = std::stoi(var.second);
		if (var.first == "GraphicLib")      config.GraphicEngine = var.second;
		if (var.first == "WallCollision")   config.WallCollision = (var.second == "1");
	}
	return config;
}

void SettingManager::SetConfigVariables(ConfigValues &conf) {

	if (SelfCheck(conf)) {
		WriteToFile(conf);
	} else throw SettingException("Error: try to write invalid config values");
}

bool SettingManager::SelfCheck(ConfigValues &conf) {
	if (conf.ScreenWidth > WIND_MAX_SIZE || conf.ScreenHeight > WIND_MAX_SIZE ||
	    conf.ScreenHeight < WIND_MIN_SIZE || conf.ScreenWidth < WIND_MIN_SIZE ||
	    conf.difficult > NIGTHMARE_MODE)
		return false;
	return "SDL" == conf.GraphicEngine || conf.GraphicEngine == "OpenGL" || conf.GraphicEngine == "SMFL";
}


