#include <utility>

//
// Created by Alexey VISHNEVSKY on 2019-10-08.
//

#include "CustomExceptions.h"

EngineException::EngineException(std::string message) : msg(std::move(message)) {}
SettingException::SettingException(const std::string &message) : logic_error(message), msg(message) {}
const char *EngineException::what() const noexcept {
    return msg.c_str();
}

const char *SettingException::what() const noexcept {
	return msg.c_str();
}

EngineException::~EngineException() noexcept = default;
SettingException::~SettingException() noexcept = default;