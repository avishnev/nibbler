//
// Created by Alexey VISHNEVSKY on 2019-10-08.
//

#include "OGL_library.h"
#include <iostream>
#include <chrono>
#include <random>

extern "C" IGraphicLib *Execute(std::shared_ptr<DrawBuffer> &config) {
	auto *graphicInterface = new GraphicEngine_OGL(config);
	return graphicInterface;
}
extern "C" void Destroy(GraphicEngine_OGL *ptr) {
	delete ptr;
}

void GraphicEngine_OGL::Draw() {
	std::cout << "Draw from OGL lib" << std::endl;
	DrawMap();
	DrawFruit();
	DrawShake();
}

void GraphicEngine_OGL::keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
	(void) mods;
	(void) scancode;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

void GraphicEngine_OGL::errorCallback(int err, const char *what) {
	if (err && what)
		std::cerr << what << std::endl;
	else
		std::cerr << "Unknown error" << std::endl;
}

GraphicEngine_OGL::GraphicEngine_OGL(std::shared_ptr<DrawBuffer>& gameData) : m_instance(gameData) { //TODO: Change arguments type -> Config
	initOpenGL();
}


void GraphicEngine_OGL::checkKeyboardEvents() {
	if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(m_window, true);
	if (glfwGetKey(m_window, GLFW_KEY_UP) == GLFW_PRESS)
		std::cout << "CHNAGE DIRECTION UP" << std::endl;
	if (glfwGetKey(m_window, GLFW_KEY_DOWN) == GLFW_PRESS)
		std::cout << "CHNAGE DIRECTION DOWN" << std::endl;
	if (glfwGetKey(m_window, GLFW_KEY_LEFT) == GLFW_PRESS)
		std::cout << "CHNAGE DIRECTION LEFT" << std::endl;
	if (glfwGetKey(m_window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		std::cout << "CHNAGE DIRECTION RIGHT" << std::endl;
}




void GraphicEngine_OGL::DrawFruit()
{
	m_instance->getFood()->GetY();
	m_instance->getFood()->GetX();
}

void GraphicEngine_OGL::DrawShake() {
	//TODO: Create vertex on Head and tail position
}

void GraphicEngine_OGL::DrawMap() {
//	if (GameData->) {
		//TODO: Draw Border
//	}
}

void GraphicEngine_OGL::initOpenGL() {

    const auto config = m_instance->getGameSetting();
	glfwSetErrorCallback(GraphicEngine_OGL::errorCallback);
	if (!glfwInit()) {
		std::cerr << "Cant init OpenGL lib" << std::endl;
		return ;
	}
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


	if (!(m_window = glfwCreateWindow(config->ScreenWidth, config->ScreenHeight,
			"Nibbler", nullptr, nullptr))) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(m_window);
	glfwSetKeyCallback(m_window, keyCallback);

	while (!glfwWindowShouldClose(m_window)) {
		checkKeyboardEvents();
//		DrawMap();
		glfwSwapBuffers(m_window);
		glfwPollEvents();

	}
}

GraphicEngine_OGL::~GraphicEngine_OGL()
{
    if (m_window)
		glfwDestroyWindow(m_window);
	glfwTerminate();
}

bool GraphicEngine_OGL::EventChecker()
{
	if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(m_window, true);
		return false;
	}
	if (glfwGetKey(m_window, GLFW_KEY_UP) == GLFW_PRESS)
		std::cout << "CHNAGE DIRECTION UP" << std::endl;
	if (glfwGetKey(m_window, GLFW_KEY_DOWN) == GLFW_PRESS)
		std::cout << "CHNAGE DIRECTION DOWN" << std::endl;
	if (glfwGetKey(m_window, GLFW_KEY_LEFT) == GLFW_PRESS)
		std::cout << "CHNAGE DIRECTION LEFT" << std::endl;
	if (glfwGetKey(m_window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		std::cout << "CHNAGE DIRECTION RIGHT" << std::endl;
	return true;
}
