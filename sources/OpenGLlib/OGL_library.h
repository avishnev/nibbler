//
// Created by Alexey VISHNEVSKY on 2019-10-08.
//

#pragma once

#include "../IGraphicLib.h"
#include "../SettingManager.h"

#include <iostream>
#include <GLFW/glfw3.h>
#include <memory>


class GraphicEngine_OGL : public IGraphicLib {

public:
	explicit GraphicEngine_OGL(std::shared_ptr<DrawBuffer>& Setting);

	~GraphicEngine_OGL() override;

	void Draw() override;
	bool EventChecker() override;

	void DrawMap() ;
	void DrawShake() ;
	void DrawFruit() ;


private:

	void initOpenGL();

	void checkKeyboardEvents();
	static void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);
	static void errorCallback(int err, const char *what);

	std::shared_ptr<DrawBuffer> m_instance;

	GLFWwindow *m_window {nullptr};
};
