//
// Created by ovishnevskiy on 10/30/19.
//

#include <random>
#include "GameLogic.h"

bool GameLogic::GameOver() const {
    return false;
}

std::string GameLogic::GetCoreLibrary() const {
    if (_GameSetting)
        return _GameSetting->GraphicEngine;
    else
        return "No engine defined";
}

void GameLogic::SetCoreLibrary(std::string lib) noexcept {
    _GameSetting->GraphicEngine = std::move(lib);
}

void GameLogic::LoadSetting(ConfigValues setting) {
	_GameSetting = std::make_shared<ConfigValues>(setting);
}

GameLogic::GameLogic() {

}

std::pair<int, int> GameLogic::GenerateRandomPosition() {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(1, _GameSetting->ScreenHeight);
//TODO: check intersection with snake
	return std::make_pair(dis(gen), dis(gen));
}
