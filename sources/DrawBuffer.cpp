//
// Created by Oleksii on 11/26/19.
//

#include "DrawBuffer.h"


void DrawBuffer::LoadSnakeModel(const Snake &ref) noexcept {
	if (!_Snake.unique())
		_Snake.reset();
	_Snake = std::make_shared<Snake>(ref);
}

void DrawBuffer::LoadGameSetting(const ConfigValues &ref) noexcept {
	if (!_GameSetting.unique())
		_GameSetting.reset();
	_GameSetting = std::make_shared<ConfigValues>(ref);
}

void DrawBuffer::LoadFoodModel(const Food &ref) noexcept {
	if (!_Fruit.unique())
		_Fruit.reset();
	_Fruit = std::make_shared<Food>(ref);
}
//
//std::shared_ptr<Food> DrawBuffer::getFood() {
//	return _Fruit;
//}
//
//std::shared_ptr<Snake> DrawBuffer::getSnake() {
//	return _Snake;
//}
//
//std::shared_ptr<ConfigValues> DrawBuffer::getGameSetting() {
//	return _GameSetting;
//}
