//
// Created by Alexey VISHNEVSKY on 2019-10-08.
//

#pragma once

#include <exception>
#include <stdexcept>
#include <string>

class EngineException : public std::exception {

public:
    [[nodiscard]] const char *what() const noexcept override;
    explicit EngineException(std::string message);
    ~EngineException() noexcept override;
private:
    const std::string msg;
};

class SettingException : public std::logic_error {
public:
	[[nodiscard]] const char *what() const noexcept override;
	explicit SettingException(const std::string &message);
	~SettingException() noexcept override;
private:
	const std::string msg;
};
