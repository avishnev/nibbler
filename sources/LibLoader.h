//
// Created by ovishnevskiy on 10/14/19.
//

#pragma once

#include <string>
#include <dlfcn.h>
#include "IGraphicLib.h"
#include "SettingManager.h"

template<class T, class D>
class LibLoader {
public:
    explicit LibLoader(std::string path, std::string init, std::string cleanup) : _path(std::move(path)),
        _init_method(std::move(init)), _cleanup_method(std::move(cleanup)) {
            _path += ostype;
        }


    bool loadLib() {
        Libptr = dlopen(_path.c_str(), RTLD_LAZY);
        return Libptr != nullptr;
    }

    bool closeLib() {
        return dlclose(Libptr) != 0;
    }

	void initMethod(std::shared_ptr<D> data) {
        using Alloc = typename T::init_t*;
        auto Exec = reinterpret_cast<Alloc>(dlsym(Libptr, _init_method.c_str()));
        Exec(data);
	}

	void drawData(std::shared_ptr<D> data) {
		using Alloc = typename T::init_t*;
		auto Exec = reinterpret_cast<Alloc>(dlsym(Libptr, _init_method.c_str()));
		Exec(data);
    }
    
	void cleanMethod() {
        using Dealloc = typename T::cleanup_t*;
        auto Clean = reinterpret_cast<Dealloc> (dlsym(Libptr, _cleanup_method.c_str()));
        Clean();
	}

	std::shared_ptr<T> getInstance(std::shared_ptr<D> data) {
    	using Init = typename  T::init_t*;

		auto Exec = reinterpret_cast<Init>(dlsym(Libptr, _init_method.c_str()));
		return std::shared_ptr<T>(Exec(data));
    }

    ~LibLoader() = default;

private:
#ifdef __linux__
    std::string ostype = ".so";
#endif
#ifdef _WIN64
    std::string ostype = ".dll";
#endif
#ifdef  __APPLE__
    std::string ostype = ".dylib";
#endif
    std::string _path;
    std::string _init_method;
    std::string _cleanup_method;
    void *Libptr = nullptr;
};
